## RASPBERRY Pi

![](image/1.JPG)


The Raspberry Pi is a very cheap computer that runs Linux, but it also provides a set of GPIO (general purpose input/output) pins that allow you to control electronic components for physical computing and explore the Internet of Things (IoT).
This was made for those countries which could not even afford computers, so it was developed as an educational tool, and has been made available to everyone , no matter your computer expertise.

![](image/2.JPG)

Figure showing us about different models and the varying size time by time.

### Components of raspberry pi

![](image/3.JPG)

### ARM CPU/GPU --

 The CPU handles all the computations that make a computer work (taking input, doing calculations and producing output), and the GPU handles graphics output.
### GPIO – 
It stands for General Purpose Input/Output. These are exposed connection points that will allow the real hardware hobbyists the opportunity to tinker.
### RCA – 
We can connect a physical camera usb but raspberry provides us an inbuilt RCA jack. An RCA jack allows connection of analog TVs and other similar output devices.
### Audio out -- 
This is a standard 3.55-millimeter jack for connection of audio output devices such as headphones or speakers. There is no audio in.
###LEDs -- 
Light-emitting diodes, for all of your indicator light needs.
### HDMI -- 
This connector allows you to hook up a high-definition television or other compatible device using an HDMI cable.
### Power -- 
This is a 5v Micro USB power connector into which you can plug your compatible power supply.
### SD card slot --
 This is a full-sized SD card slot. Used to download an OS and save it to the card yourself if you have a Linux machine and the wherewithal.
### Ethernet --
 This connector allows for wired network access and is only available on the Model B.The latest models are given WiFi.

![](image/4.JPG)

Some OS widely used in raspberry pi are shown above.


