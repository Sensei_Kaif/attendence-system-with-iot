## Faster Approach towards Learning:- 20Hrs learning

Why learning is so important ?How to learn ? What time it takes to learn a new skill ? etc we have many questions like this, but rather than giving the answers of such questions lets see what actually learning is .

### Learning  :-  

Learning is the process of acquiring new understanding ,knowledge, behavior ,skills, values, attitudes, and preference.

### Skills :-

A skill is the ability to perform an action with determined results often within a given amount of time, energy, or both. 
The ability to learn is possessed by humans, animals, and some machines; there is also evidence for some kind of learning in certain plants.
We have to learn skills because if we don’t do so we will lack behind the world and being a developing country we have to work hard .

Are main topic of discussion is about “How much time is required to learn a new skill ??? “. 

![](image/kaif1.JPG)   

This graph is made by researchers , about the time taken in learning skill and the performance level. They were focused on performance and not on time taken which means as long you practice the good you will be at the work.

![](image/kaif2.JPG)   

This the learning curve, telling us about we learn time-by-time.

The answer comes out be 20 hrs by – JOSH KAUFMAN.
He researched that If you put 20 hours of focused deliberate practice into that thing.
 
There are 4 ways to learn the skill in this way. That are :-
 
 
* We have to deconstruct the skills.

We have to break the big skill into small to master the overall skill nicely. Some skills requires mastery of small skills.

* The second is to learn enough, to self correct. 

You can actually practice and self correct or self edit as you practice. So the learning becomes a way of getting better.

* The third is to remove barriers in learning 

To remove the barriers that are keeping you away from learning such as Television , social media etc.

* To practice for atleast 20 hrs      
 
### THE MOST MOTIVATING LINE HE TOLD WAS
 
![](image/kaif3.JPG)                                  
 
### THIS WILL HELP US TO PERPETRATE LEARNING.                                                     
