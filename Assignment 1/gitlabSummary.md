## GITLAB
                
### What is GitLab?

![img](image/kaif16.JPG)

GitLab is a complete DevOps platform, delivered as a single application. This makes GitLab unique and makes Concurrent DevOps possible, unlocking your organization from the constraints of a pieced together toolchain. 
In easy words, it means that gitlab is a platform for a team to work.
let’s see how it works :

![img](image/kaif4.JPG)

The software was created by Ukrainians Dmitriy Zaporozhets and Valery Sizov, these are the same guys who made LINUX KERNEL. Basically,Linux is an open source so if you are anywhere in the world you can contribute to it or in other words you are the contributor of it. 
Similarly, if a project is going onn and you have a something new that will help in the project then you can merge your work with them or you too contribute.

### SOME IMPORTANT TERMINOLOGY TO BE KNOWN :-

![img](image/kaif5.JPG)

### Repository:
It is collection of files and folders which is used gitlab to track for.It consists the entire history of the changes made during the project.It is the central main file for which your team work for.

### Fork:
A fork is a copy of original repo that another teammate uses ,where they use it and apply changes, without affecting the original file.

### Clone:
Cloning a repo is pretty much exactly what it sounds like. It takes the entire online repository and makes an exact copy of it on your local machine.
 
### Merge:
When the team perpetrate the project and succeed or make a beneficial change in the task then it request the master ,for merging with the original file.

![img](image/kaif6.JPG)
 
### Branch:
A branch is a version of a project’s working tree. You create a branch for each set of related changes you make. This keeps each set of changes separate from each other, allowing changes to be made in parallel, without affecting each other.


### Push:
The push command is used to upload local repository content to a remote repository. Pushing is how you transfer commits from your local repository to a remote repo.

### Commit:
A commit  adds the latest changes to [part of] the source code to the repository, making these changes part of the head revision of the repository. 

### Pull Request:
Pull requests let you tell others about changes you've pushed to a branch in a repository.


### Creating a fork
Forking a project is, in most cases, a two-step process.

1. On the project’s home page, in the top right, click the Fork button.

![img](image/kaif7.JPG)

2. Click a namespace to fork to. Only namespaces you have Developer and higher permissions for are shown.
Note: The project path must be unique within the namespace

![img](image/kaif8.JPG)

 
3. The fork is created. The permissions you have in the namespace are the permissions you will have in the fork.
 
 
 

2. Clone Repository
2.1 Launch GitHub Desktop and tap the "File" menubar item and choose "Clone Repository..."

![img](image/kaif9.JPG)

2.2 Tap on the "URL" tab

![img](image/kaif10.JPG)

2.3 Paste the previously copied HTTPS URL into the repository URL field

![img](image/kaif11.JPG)

2.4 Tap the "Clone" button

![img](image/kaif12.JPG)

2.5 Tap the "Repository" menubar item and view it's local folder

![img](image/kaif13.JPG)

On a Mac, choose "Show in Finder"
On Windows, choose "Show in Explorer"

2.6 Review the files of your cloned GitLab repository.

![img](image/kaif14.JPG)

You can now edit these files locally, and use GitHub Desktop to push changes to your GitLab repository.


### Merging upstream


When you are ready to send your code back to the upstream project, create a merge request. For Source branch, choose your forked project’s branch. For Target branch, choose the original project’s branch.

![img](image/kaif15.JPG)

Then you can add labels, a milestone, and assign the merge request to someone who can review your changes. Then click Submit merge request to conclude the process. When successfully merged, your changes are added to the repository and branch you’re merging into.s
